cd ..
mkdir -p ./openssl-install-org
mkdir -p ./openssl-build-org
cd openssl-build-org
../openssl/Configure --prefix=$PWD/../openssl-install-org --openssldir=$PWD/../openssl-install-org/etc/ssl --libdir=lib shared linux-x86_64 "-Wa,--noexecstack -fcf-protection -mshstk $CPPFLAGS $CFLAGS $LDFLAGS"
make -j depend
make -j